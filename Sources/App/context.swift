import Foundation
import Vapor

struct LContext: Codable {
    let config: Config
    let loggedIn: Bool
}