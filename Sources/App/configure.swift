import Fluent
import Vapor
import Leaf
import LeafErrorMiddleware
import FluentSQLiteDriver

// configures your application
public func configure(_ app: Application) throws {
    app.middleware.use(LeafErrorMiddleware())
    // Serve files from /Public
    app.middleware.use(FileMiddleware(publicDirectory: app.directory.publicDirectory))

    app.sessions.use(.fluent)
    app.sessions.configuration.cookieName = "statusboard-session"
    app.sessions.configuration.cookieFactory = { sessionID in
        .init(string: sessionID.string, expires: Date(timeIntervalSinceNow: 60*60*24*365), isSecure: true, isHTTPOnly: true, sameSite: HTTPCookies.SameSitePolicy.none)
    }
    app.middleware.use(app.sessions.middleware)

    app.databases.use(.sqlite(.file("\(app.directory.resourcesDirectory)/db.sqlite")), as: .sqlite)
    app.migrations.add(SessionRecord.migration)

    // Configure Leaf
    LeafOption.caching = app.environment.isRelease ? .default : .bypass
    LeafRenderer.Option.timeout = 200.0
    app.views.use(.leaf)

    // Register routes
    try routes(app)
}
