import Foundation
import Vapor

struct Config: Codable {
    let title: String
    let passwordHash: String
    let services: [String:[[String:String]]]
}